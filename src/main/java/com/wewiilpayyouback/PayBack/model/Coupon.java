package com.wewiilpayyouback.PayBack.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "COUPONS", indexes = @Index(columnList = "member_id"))
public class Coupon {

    @Column(name = "member_id")
    @NotNull Long memberId;
    @Id
    @Column(name = "coupon_id")
    @NotNull Long couponId;
    @NotNull
    @Column(name = "valid_from")
    LocalDate validFrom;
    @NotNull
    @Column(name = "valid_until")
    LocalDate validUntil;

    public Coupon() {
    }

    public Coupon(long memberId, long couponId, LocalDate validFrom, LocalDate validUntil) {
        this.memberId = memberId;
        this.couponId = couponId;
        this.validFrom = validFrom;
        this.validUntil = validUntil;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coupon coupon = (Coupon) o;
        return Objects.equals(memberId, coupon.memberId) && Objects.equals(couponId, coupon.couponId) && Objects.equals(validFrom, coupon.validFrom) && Objects.equals(validUntil, coupon.validUntil);
    }

    @Override
    public int hashCode() {
        return Objects.hash(memberId, couponId, validFrom, validUntil);
    }

    public long getMemberId() {
        return memberId;
    }

    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }

    public long getCouponId() {
        return couponId;
    }

    public void setCouponId(long couponId) {
        this.couponId = couponId;
    }

    public LocalDate getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDate validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDate getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(LocalDate validUntil) {
        this.validUntil = validUntil;
    }
}
