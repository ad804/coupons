package com.wewiilpayyouback.PayBack.repository;

import com.wewiilpayyouback.PayBack.model.Coupon;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface CouponRepository extends CrudRepository<Coupon, Long> {

    @Query("SELECT c FROM Coupon c  WHERE c.memberId = :memberId " +
            " AND c.validFrom <= :currentDate " +
            "AND c.validUntil >= :currentDate " +
            "order by c.validUntil desc")
    List<Coupon> findActiveCouponsForMemberId(@Param("memberId") Long memberId, @Param("currentDate") LocalDate date);
}
