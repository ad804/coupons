package com.wewiilpayyouback.PayBack.controller;

import com.wewiilpayyouback.PayBack.model.Coupon;
import com.wewiilpayyouback.PayBack.service.CouponService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/getMemberCoupons")
public class CouponsController {

    private final CouponService service;

    public CouponsController(CouponService service) {
        this.service = service;
    }

    @GetMapping("/{memberId}")
    public List<Coupon> getCouponsForMember(@PathVariable long memberId) {
        return service.getCouponsForMember(memberId);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Coupon createNewCoupon(@Valid Coupon coupon) {
        return service.save(coupon);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/{couponId}")
    public Coupon replaceCoupon(@RequestBody @Valid Coupon coupon, @PathVariable long couponId) {
        return service.replaceCoupon(coupon, couponId);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{couponId}")
    public void delete(@PathVariable long couponId) {
        service.delete(couponId);
    }
}
