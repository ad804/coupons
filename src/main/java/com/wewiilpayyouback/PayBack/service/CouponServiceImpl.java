package com.wewiilpayyouback.PayBack.service;

import com.wewiilpayyouback.PayBack.model.Coupon;
import com.wewiilpayyouback.PayBack.repository.CouponRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class CouponServiceImpl implements CouponService {

    private final CouponRepository repository;

    public CouponServiceImpl(CouponRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Coupon> getCouponsForMember(Long memberId) {
        return repository.findActiveCouponsForMemberId(memberId, LocalDate.now());
    }

    public Coupon replaceCoupon(Coupon coupon, long couponId) {
        return repository.findById(couponId).map(existingCoupon -> {
            existingCoupon.setMemberId(coupon.getMemberId());
            existingCoupon.setValidFrom(coupon.getValidFrom());
            existingCoupon.setValidUntil(coupon.getValidUntil());
            return repository.save(existingCoupon);
        }).orElseGet(() -> {
            coupon.setCouponId(couponId);
            return repository.save(coupon);
        });
    }

    public void delete(long couponId) {
        repository.deleteById(couponId);
    }

    @Override
    public Coupon save(Coupon coupon) {
        return repository.save(coupon);
    }
}
