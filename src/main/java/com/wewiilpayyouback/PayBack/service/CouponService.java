package com.wewiilpayyouback.PayBack.service;

import com.wewiilpayyouback.PayBack.model.Coupon;

import java.util.List;

public interface CouponService {

    List<Coupon> getCouponsForMember(Long memberId);

    Coupon replaceCoupon(Coupon coupon, long couponId);

    void delete(long couponId);

    Coupon save(Coupon coupon);
}
